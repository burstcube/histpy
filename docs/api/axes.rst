Axes
----
      
.. autoclass:: histpy.Axes
   :show-inheritance:
   :members:
   :inherited-members:
