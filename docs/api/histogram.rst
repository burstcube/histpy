Histogram
----------
      
.. autoclass:: histpy.Histogram
   :show-inheritance:
   :members:
   :inherited-members:
