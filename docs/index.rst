Welcome to histpy's documentation!
==================================

The *histpy* library provides a :doc:`Histogram <api/histogram>` class which is, in essence, an array with axes attached defining the value of bin boundaries. It can handle an arbitrary number of dimensions, be accessed in a similar way as a numpy array, be projected, sliced, multiplied, fitted, plotted, etc. The histograms can be weighted and they will propagate errors during all operations. The under and overflow contents are tracked. Sparse histograms are also supported. The *histpy*'s Histogram class is loosely based on a ROOT histogram but using a more pythonic interface.

See the :doc:`quick start tutorial <tutorials/quick-start>` to get started and continue to the :doc:`API <api/index>` documentation for more details.

Installation
------------

Run::
  
  pip install histpy

Alternatively, developers can get a working version with::
  
  git clone git@gitlab.com:burstcube/histpy.git
  cd  histpy
  python setup.py develop


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorials/quick-start.ipynb
   api/index
